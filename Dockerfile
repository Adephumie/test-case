# AS <NAME> to name this stage as maven
FROM maven:3.6.3 AS maven

WORKDIR /usr/src/app
COPY . /usr/src/app
# Compile and package the application to an executable JAR

RUN mvn clean package

# For Java 11, 
FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /app

# Copy the blaze-app-0.0.1-SNAPSHOT.jar from the maven stage to the /app directory of the current stage.
COPY --from=maven /usr/src/app/target/blaze-app-0.0.1-SNAPSHOT.jar /app/

EXPOSE 8081

CMD ["java", "-jar", "blaze-app-0.0.1-SNAPSHOT.jar"]