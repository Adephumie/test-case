package com.funmifago.blazeapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.funmifago.controller.HelloWorldController;

@SpringBootApplication
@ComponentScan(basePackageClasses = HelloWorldController.class)
public class BlazeAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlazeAppApplication.class, args);
	}

}
