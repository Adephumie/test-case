<b><h1> FULL CI-CD-Jenkins Project Setup on Local Machine</h1></b>


## 1. Fork the Repository:

- Copy this [link](https://gitlab.com/Adephumie/my-blaze-app) to your  browser to access my repo and  fork it. Then, click on the clone button to copy the url of your new repository.

![fork-annd-clone](images/fork-clone.png "fork-and-clone")

- On your terminal, create a new workspace or folder to start your project and pull the repo to your local machine, however, initialize git before proceeding to pull the repo.
  
  ```
  git init
  git pull <REPO-URL>
  ```



## 2. Provisioning Jenkins Server

Install Jenkins on your machine depending on your OS, look through the documentation [here](https://www.jenkins.io/download/) to set it up. I will give the steps on how I provisioned my Jenkins server on both MacOs with jenkins.war file and on [Ubuntu 20.04 VM](#installing-with-debian-packages-on-ubuntu-2004) with Jenkins Debian package.  

### ===> Installing With Jenkins.war File on MacOS


- The `jenkins.war` file requires Java to run, so you should have Java installed on your system.

- On your web browser, visit the [jenkins download page](https://www.jenkins.io/download/) and click on the Generic Java Package `.war` file (LTS) to automatically download to your system. You can create a folder and move the file into it.
  

![jenkins.war_download](images/jenkins-war-file.png "jenkins.war_download")

- Then, from your terminal, `cd` into the directory holding the `jenkins.war` file and run:

  ```
  java -jar jenkins.war
  ```

- When the process completes, on the terminal, you will see an auto-generated password. Copy it and go to your browser.

- On your web browser, go to `http://localhost:8080`  to access the Jenkins Admin page. In the Administrator password field, paste the random password you copied in the previous step.
  

![jenkins-administrator-page](images/jenkins-admin.png "jenkins-administrator-page")

- To continue the Installation process, you need to install necessary plugins, create the first admin with password and finish the installation process by clicking `yes` till you are taken to the dashboard.

![jenkins-installation](images/jenkins-installation.png "jenkins-installation")

![jenkins-admin](images/create-jenkin-admin.png "jenkins-admin").

![jenkins-dashboard](images/jenkins-dashboard.png "jenkins-dashboard")

<br>

### ===> Installing With Debian Packages on Ubuntu 20.04

<br>

- On this [Jenkins Debian Package](https://pkg.jenkins.io/debian-stable/) page, you will find commands to install openjdk-11-jre and jenkins packages. Note that if you already have Java on your system, you should skip the java installation part.
  
- On your web browser, go to `http://localhost:8080`  to access the Jenkins Admin page.
  

![jenkins-administrator-page](images/jenkins-admin.png "jenkins-administrator-page")

- On the Jenkins Administrator's page, there is a path to a secret file holding the password. Copy the file path and on your terminal, type:
  
```
sudo cat <FILE-PATH>
```

- This will print out the secret password on your terminal, copy and paste it back into the `administrator password` field. 

- Then, install suggested plugins and create first admin users as shown below.
- 
![jenkins-installation](images/jenkins-installation.png "jenkins-installation")

![jenkins-admin](images/create-jenkin-admin.png "jenkins-admin")

- Continue with the installation process till you get to the dashboard as shown above.


## 3. Integrating Gitlab with Jenkins Server

This process entails different steps needed to set up the Jenkins server to integrate with Gitlab. 

### ===> Installing Gitlab and Docker Plugins

On Jenkins Dashboard, Click on <b>Manage Jenkins</b> ----> <b>System Configuration</b> ----> <b>Plugins</b> ----> <b>Available plugins</b>

In the search field, type Gitlab and enable the following plugins: 

- Gitlab
- Gitlab API
  

Then, search for Docker in the field and enable the following:

- Docker
- Docker Commons
- Docker Pipeline
- Docker API
- docker-build-step
  

![Jenkins-plugins](images/jenkins-plugins.png "Jenkins-plugins")

Click on Download now and install after restart. On the next page, scroll all the way down and enable the `Restart Jenkins` part.

![install-plugins](images/install-plugins.png "install-plugins")

### ===> Create a Personal Access Token(PAT) on Gitlab
On the topmost part of the left pane of your Gitlab Account, Click on your account to access your profile settings dropdown. Then, click on <b>Edit Profile</b> ----> <b>Access Tokens</b> 

Create an Access Token by specifying the name, expiration date, and enabling `api` in the scope section. Create the Token and copy it.

![PAT](images/PAT.png "PAT")

<br>

### ===> Connect Gitlab API to Jenkins with PAT
On the left pane of Jenkins dashboard, Click on <b>Manage Jenkins</b> ----> <b>System Configuration</b> ----> <b>System</b> 

Scroll down to <b>Gitlab</b> section 

![Gitlab-to-jenkins-authentication](images/Gitlab-connect-jenkin.png "Gitlab-to-jenkins-authentication")

For the form fields:
Connection name = Give a descriptive name
Gitlab host URL = https://gitlab.com

To add the credentials, Click on <b>add</b> to go to the credentials creation page.

In the Jenkins Credentials Provider Fields:
Kind = GitLAb API token
API token = Paste your PAT generated above
ID = A descriptive name as shown
Description = Describe the key
Then add.

![gitlab-login-access](images/gitlab-login.png "gitlab-login-access")

Now click again to add the newly created credential, click on test connection and it should return <b>Success</b>.

![gitlab-API-connected](images/gitlab-api-connected.png "Gitlab-API-connected")

Finally, Click on Save at the bottom of the page to save the configuration.



### ===> Create and Configure  Jenkins 

On the dashboard, Click on <b>New Item</b> ----> <b>Enter a new project name</b> ----> <b>Multibranch Pipeline</b> ----> <b>Ok</b>

In the Project Configuration settings page, Scroll down to <b>Branch Source</b> and in the drop-down tab, click on <b>Git</b> to expand the block.

Go to your Gitlab Account and in the <b>Clone</b> dropdown, copy your <b>SSH URL</b>.

![ssh-url](images/ssh-url.png "ssh-url")

Copy the Git repository from git lab clone section and paste into the Jenkins multibranch pipeline we are trying to configure.

### NOTE:

In this step, you can copy and paste the <b>https URL</b> to connect with the jenkins server if you have a password generated with your gitlab username. Then, you will pick `Username With Password` Option when setting the credentials for your Jenkins server. My gitlab account was created using github for authentication, so with no password, I decided to use `ssh`.

Setting up SSH authentication for gitlab.
If you are working with a machine that has already been synced with your gitlab account, then you don't need this step. 

So, to walk you through this setup:

- On your terminal, create an `ssh key pair` and to keep things simple don't add any passphrase. 

```
ssh-keygen 
```
- Expose the content of the public folder and copy it.
  
  ```
  cat ~/.ssh/id_rsa.pub
  ```

- On your gitlab account, Go to <b>edit profile</b>, on the left side of the page, click on <b>SSH keys</b>

- Paste the public key you copied from your terminal into the <b>key field</b> and fill the other information then save.

Then, on Jenkins server, still on the configuration page, click on <b>Add</b> ----> <b>Jenkins</b>. In the Jenkins Credentials Provider Fields:

Kind = SSH Username with private key
ID = A descriptive name as shown
Description = Describe the key
Username = Gitlab Username
Private Key = Copy the ssh private key from the ssh keys pair that you generated before.

To get this, On your terminal, type:
```
cat ~/.ssh/id_rsa
```

Then Add the credentials.

![gitlab-ssh-key](images/gitlab-ssh-key.png "gitlab-ssh-key")


Still on the multibranch pipeline configuration page, Proceed to Add Filter by name (with regular expression) in the <b>Discover Branches</b> sub-section. 

![discover-branch](images/discover-branch.png "discover-branch")

The default value of `.*` implies that as it is. But to explicitly set the the specific branch we want to take effect, we will change the value to: `^main|stage|dev$` as shown.

![discover-specific-branch](images/regular-expression.png "discover-specific-branch")

Leave the default value for the `Build Configuration` as `Jenkinsfile`.

In the <b>Orphaned Item Strategy</b> section, input 3 in the <b>Max # of old Items to keep</b>. This will help to save space by keeping record of last three builds.

![maximum-build](images/max-build.png "maximum-build")

More in my repo.

More testing






















